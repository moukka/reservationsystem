package reservationsystem;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author markus
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private ComboBox<String> listOfReservers;
    @FXML
    private Button createReserverButton;
    @FXML
    private TextField reserverName;
    @FXML
    private TextField reserverAddress;
    @FXML
    private TextField reserverPN;
    @FXML
    private Label reserverInfo;
    @FXML
    private DatePicker calendar1;
    @FXML
    private Label label1;
    @FXML
    private Button button1;
    @FXML
    private CheckBox coffeeR1;
    @FXML
    private CheckBox waterR1;
    @FXML
    private CheckBox laptopR1;
    @FXML
    private TextField nrOfAttendeesR1;
    @FXML
    private Button showButtonR1;
    @FXML
    private Button cancelButtonR1;
    @FXML
    private DatePicker calendar2;
    @FXML
    private Label label2;
    @FXML
    private Button button2;
    @FXML
    private CheckBox coffeeR2;
    @FXML
    private CheckBox waterR2;
    @FXML
    private CheckBox laptopR2;
    @FXML
    private TextField nrOfAttendeesR2;
    @FXML
    private Button showButtonR2;
    @FXML
    private Button cancelButtonR2;
    

    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        RoomDB roomDB = new RoomDB();
        Room r1 = roomDB.getRoom(0);
        LocalDate date = calendar1.getValue();
        String name = listOfReservers.getValue();
        String attendees = nrOfAttendeesR1.getText();
        int numberOfAttendees;
        try {
            numberOfAttendees = Integer.parseInt(attendees);
        }catch (NumberFormatException e) {
            numberOfAttendees = -1;
        }
        if (date != null && name != null && numberOfAttendees != -1){
            boolean temp = r1.addRoomReservation(date);
            
            
            if (temp)
            {
                boolean coffee = coffeeR1.isSelected();
                boolean water = waterR1.isSelected();
                boolean laptop = laptopR1.isSelected();
                Reservation reservation = Reservation.getInstance();
                ReservationDetails rd = new ReservationDetails(name, date, coffee, water, laptop, numberOfAttendees, r1);
//                reservation.addReservation(name, date, coffee, water, laptop, numberOfAttendees, r1);
                reservation.addReservation(rd);
                int i = reservation.getSize();
                label1.setText("Varaus nimellä " + reservation.getReservationDetails(i-1).getName() + " tehty!");
            }else{
                label1.setText("Huone on jo varattu tuona päivänä");
            }
        }else{
            label1.setText("Varaus epäonnistui! Varmista, että varaaja, varauspäivämäärä ja osallistujen lukumäärä on valittu!");
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        RoomDB roomDB = new RoomDB();
        ReserverDB reserverDB = new ReserverDB();
        
        
        
        Room r1 = new Room("Kokoushuone 1");
        roomDB.addRoom(r1);
        Room r2 = new Room("Kokoushuone 2");
        roomDB.addRoom(r2);

        
        Reserver reserver1 = new Reserver ("Erkki Esimerkki", "0443090002", "asdasd");
        reserverDB.addReserver(reserver1);
        Reserver reserver2 = new Reserver ("Mikki Hiiri", "0443090003", "asdasd");
        reserverDB.addReserver(reserver2);
        
        
        
        
        for (int i=0; i<reserverDB.getReservers().size(); i++)
        {
            listOfReservers.getItems().add(reserverDB.getReservers().get(i).getName());
        }
    }    

    @FXML
    private void handleButton2Action(ActionEvent event) {
        RoomDB roomDB = new RoomDB();
        Room r1 = roomDB.getRoom(1);
        LocalDate date = calendar2.getValue();
        String name = listOfReservers.getValue();
        String attendees = nrOfAttendeesR2.getText();
        int numberOfAttendees;
        try {
            numberOfAttendees = Integer.parseInt(attendees);
        }catch (NumberFormatException e) {
            numberOfAttendees = -1;
        }
        if (date != null && name != null && numberOfAttendees != -1){
            boolean temp = r1.addRoomReservation(date);
            
            
            if (temp)
            {
                boolean coffee = coffeeR2.isSelected();
                boolean water = waterR2.isSelected();
                boolean laptop = laptopR2.isSelected();
                Reservation reservation = Reservation.getInstance();
                ReservationDetails rd = new ReservationDetails(name, date, coffee, water, laptop, numberOfAttendees, r1);
//                reservation.addReservation(name, date, coffee, water, laptop, numberOfAttendees, r1);
                reservation.addReservation(rd);
                int i = reservation.getSize();
                label2.setText("Varaus nimellä " + reservation.getReservationDetails(i-1).getName() + " tehty!");
            }else{
                label2.setText("Huone on jo varattu tuona päivänä");
            }
        }else{
            label2.setText("Varaus epäonnistui! Varmista, että varaaja, varauspäivämäärä ja osallistujen lukumäärä on valittu!");
        }
    }

    @FXML
    private void createReserver(ActionEvent event) {
        ReserverDB reserverDB = new ReserverDB();
        if (!reserverName.getText().equals("") && !reserverAddress.getText().equals("") && !reserverPN.getText().equals(""))
        {
            Reserver reserver = new Reserver(reserverName.getText(), reserverPN.getText(), reserverAddress.getText());
            reserverDB.addReserver(reserver);
            reserverInfo.setText("Uuden varaajan luonti onnistui!");
            listOfReservers.getItems().add(reserver.getName());
        }else{
            reserverInfo.setText("Varmista että täytit kaikki kentät!");
        }
    }

    @FXML
    private void showEventR1(ActionEvent event) {
        RoomDB roomDB = new RoomDB();
        ReserverDB reserverDB = new ReserverDB();
        Reservation reservation = Reservation.getInstance();
        
        Room room = roomDB.getRoom(0);  //Conference room 1
        LocalDate date = calendar1.getValue();
        System.out.println(room.getRoomID() + date);
        int index = reservation.findReservation(room, date);
        
        if (index != -1){
            ReservationDetails reservationDetails = reservation.getReservationDetails(index);
            String itemsRequired;
            //Determining which services are needed for the reservation.
            if (reservationDetails.getCoffee() == true && reservationDetails.getWater() == true && reservationDetails.getLaptop() == true)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille kahvia, vettä ja kanettavat tietokoneet.";
            }else if (reservationDetails.getCoffee() == true && reservationDetails.getWater() == true && reservationDetails.getLaptop() == false)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille kahvia ja vettä.";
            }else if (reservationDetails.getCoffee() == true && reservationDetails.getWater() == false && reservationDetails.getLaptop() == true)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille kahvia ja kanettavat tietokoneet.";
            }else if (reservationDetails.getCoffee() == false && reservationDetails.getWater() == true && reservationDetails.getLaptop() == true)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille vettä ja kanettavat tietokoneet.";
            }else if (reservationDetails.getCoffee() == false && reservationDetails.getWater() == false && reservationDetails.getLaptop() == false)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille kanettavat tietokoneet.";
            }else if (reservationDetails.getCoffee() == true && reservationDetails.getWater() == false && reservationDetails.getLaptop() == false)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille kahvia.";
            }
            else if (reservationDetails.getCoffee() == false && reservationDetails.getWater() == true && reservationDetails.getLaptop() == false)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille vettä.";
            }else
            {
                itemsRequired = "Kokoushuoneeseen on varattu sellaisenaan";
            }
            int temp = reserverDB.findReserver(reservationDetails.getName()); //Finding the reserver to print out his information
            Reserver reserver = reserverDB.getReserver(temp);
            label1.setText("Kokoushuone 1 on varattu " + reservationDetails.getDate() + " " + reservationDetails.getNrOfAttendees() + " osallistujalle." + 
                    " Varaajan tiedot: " + reserver.getName() + ", " 
                    + reserver.getAddress() + ", " + reserver.getPhoneNumber()
                    + ".\n" + itemsRequired);
        }else{
            label1.setText("Kokoushuonetta 1 ei ole varattu valitulle päivälle!" + date);
        }
    }

    @FXML
    private void cancelEventR1(ActionEvent event) {
        RoomDB roomDB = new RoomDB();
        ReserverDB reserverDB = new ReserverDB();
        Reservation reservation = Reservation.getInstance();
        
        Room room = roomDB.getRoom(0);  //Conference room 1
        LocalDate date = calendar1.getValue(); 
        int index = reservation.findReservation(room, date);
        
        if (index != -1)
        {
            int temp = room.findReservation(date);
            reservation.removeReservationDetails(index);
            room.removeRoomReservation(temp);
            label1.setText("Varaus poistettu onnistuneesti!");
        }else{
            label1.setText("Valitulle päivälle ei ole varauksia joita poistaa!");
        }
    }

    @FXML
    private void showEventR2(ActionEvent event) {
        RoomDB roomDB = new RoomDB();
        ReserverDB reserverDB = new ReserverDB();
        Reservation reservation = Reservation.getInstance();
        
        Room room = roomDB.getRoom(1);  //Conference room 2
        LocalDate date = calendar2.getValue();
        System.out.println(room.getRoomID() + date);
        int index = reservation.findReservation(room, date);
        
        if (index != -1){
            ReservationDetails reservationDetails = reservation.getReservationDetails(index);
            String itemsRequired;
            //Determining which services are needed for the reservation.
            if (reservationDetails.getCoffee() == true && reservationDetails.getWater() == true && reservationDetails.getLaptop() == true)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille kahvia, vettä ja kanettavat tietokoneet.";
            }else if (reservationDetails.getCoffee() == true && reservationDetails.getWater() == true && reservationDetails.getLaptop() == false)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille kahvia ja vettä.";
            }else if (reservationDetails.getCoffee() == true && reservationDetails.getWater() == false && reservationDetails.getLaptop() == true)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille kahvia ja kanettavat tietokoneet.";
            }else if (reservationDetails.getCoffee() == false && reservationDetails.getWater() == true && reservationDetails.getLaptop() == true)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille vettä ja kanettavat tietokoneet.";
            }else if (reservationDetails.getCoffee() == false && reservationDetails.getWater() == false && reservationDetails.getLaptop() == false)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille kanettavat tietokoneet.";
            }else if (reservationDetails.getCoffee() == true && reservationDetails.getWater() == false && reservationDetails.getLaptop() == false)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille kahvia.";
            }
            else if (reservationDetails.getCoffee() == false && reservationDetails.getWater() == true && reservationDetails.getLaptop() == false)
            {
                itemsRequired = "Kokoushuoneeseen on varattu kaikille osallistujille vettä.";
            }else
            {
                itemsRequired = "Kokoushuoneeseen on varattu sellaisenaan";
            }
            int temp = reserverDB.findReserver(reservationDetails.getName()); //Finding the reserver to print out his information
            Reserver reserver = reserverDB.getReserver(temp);
            label2.setText("Kokoushuone 2 on varattu " + reservationDetails.getDate() + " " + reservationDetails.getNrOfAttendees() + " osallistujalle." + 
                    " Varaajan tiedot: " + reserver.getName() + ", " 
                    + reserver.getAddress() + ", " + reserver.getPhoneNumber()
                    + ".\n" + itemsRequired);
        }else{
            label2.setText("Kokoushuonetta 2 ei ole varattu valitulle päivälle!" + date);
        }
    }

    @FXML
    private void cancelEventR2(ActionEvent event) {
        RoomDB roomDB = new RoomDB();
        ReserverDB reserverDB = new ReserverDB();
        Reservation reservation = Reservation.getInstance();
        
        Room room = roomDB.getRoom(1);  //Conference room 2
        LocalDate date = calendar2.getValue(); 
        int index = reservation.findReservation(room, date);
        
        if (index != -1)
        {
            int temp = room.findReservation(date);
            reservation.removeReservationDetails(index);
            room.removeRoomReservation(temp);
            label2.setText("Varaus poistettu onnistuneesti!");
        }else{
            label2.setText("Valitulle päivälle ei ole varauksia joita poistaa!");
        }
    }
    
}
