package reservationsystem;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;



/**
 *
 * @author markus
 */
public class Reservation {
    


    static private Reservation reservation = null; //singleton pattern
    static public Reservation getInstance(){
        if (reservation==null)
            reservation = new Reservation();
        return reservation;
    }
    private ArrayList<ReservationDetails> reservations = new ArrayList();

    public void addReservation (ReservationDetails rd)
    {
        reservations.add(rd);
        System.out.println(reservations.size());
    }    
    public int findReservation (Room r, LocalDate d)
    {
        for (int i = 0; i<reservations.size(); i++){
            if (reservations.get(i).getRoom().equals(r) && reservations.get(i).getDate().equals(d))
                return i;
        }
        return -1;
    }
    
        public ReservationDetails getReservationDetails (int i)
    {
        return reservations.get(i);
    }
        public int getSize()
        {
            return reservations.size();
        }
        
        public void removeReservationDetails (int i)
        {
            reservations.remove(i);
        }
}
class ReservationDetails extends Reservation {

    public LocalDate getDate() {
        return date;
    }

    public Room getRoom() {
        return room;
    }

    public Boolean getCoffee() {
        return coffee;
    }

    public Boolean getWater() {
        return water;
    }

    public int getNrOfAttendees() {
        return nrOfAttendees;
    }
    private String name;

    public String getName() {
        return name;
    }

    public Boolean getLaptop() {
        return laptop;
    }
    private LocalDate date;
    private Room room;
    private Boolean coffee;
    private Boolean water;
    private Boolean laptop;
    private int nrOfAttendees;
    
    
    
    public ReservationDetails (String n, LocalDate d, boolean c, boolean w, boolean l, int nrA, Room r)
    {
        name = n;
        date = d;
        room = r;
        coffee = c;
        laptop = l;
        water = w;
        nrOfAttendees = nrA;
    }
}