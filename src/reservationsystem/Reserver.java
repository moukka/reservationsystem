package reservationsystem;

import java.util.ArrayList;

/**
 *
 * @author markus
 */
public class Reserver {

    private String name;
    private String phoneNumber;
    private String address;
    
    public Reserver (String n, String pn, String a)
    {
         name = n;
         phoneNumber = pn;
         address = a;
         
         
    }
    
    public void editPhoneNumber (String pn)
    {
        phoneNumber = pn;
    }
    
    public void editAddress (String a)
    {
        address = a;
    }
    
    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    
}