package reservationsystem;

import java.util.ArrayList;

/**
 *
 * @author marku
 */
public class ReserverDB {
    private static ArrayList<Reserver> reservers = new ArrayList();

    public static ArrayList<Reserver> getReservers() {
        return reservers;
    }
    
    public ReserverDB() {
    
    }
    
    public void addReserver(Reserver r)
    {
        reservers.add(r);
    }
    public Reserver getReserver (int i)
    {
        return reservers.get(i);
    }
    
    public int findReserver (String name)
    {
        for (int i=0; i<reservers.size(); i++){
            if (reservers.get(i).getName().equals(name)){
                return i;
            }
        }
        return -1;
    }
    
    
}
