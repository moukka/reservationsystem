package reservationsystem;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author markus
 */
public class Room {
    private String roomID;

    public String getRoomID() {
        return roomID;
    }
    
    private ArrayList<LocalDate> datesReserved = new ArrayList();

    public ArrayList<LocalDate> getDatesReserved() {
        return datesReserved;
    }
    
    public boolean addRoomReservation (LocalDate d)
    {
        boolean reserved = false;
        int size = datesReserved.size();
        for (int i = 0; i<size; i++) 
        {
            if (datesReserved.get(i).equals(d)){
                reserved = true;
                break;
            }else{
                reserved = false;
            }
        
        }
        if (reserved == false)
        {
            datesReserved.add(d);
            return true;
        }else{
            return false;
        }
        
    }
    
    public void removeRoomReservation (int i)
    {
        datesReserved.remove(i);
    }
    
    public int findReservation (LocalDate d)
    {
        for (int i = 0; i<datesReserved.size(); i++){
            if (datesReserved.get(i).equals(d))
                return i;
        }  
        return -1;
    }
    
    public Room(String rID){
        roomID = rID;
    }
    

       
}
