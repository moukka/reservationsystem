package reservationsystem;

import java.util.ArrayList;

/**
 *
 * @author marku
 */
public class RoomDB {
    private static ArrayList<Room> rooms = new ArrayList();
    
    public RoomDB() {
    
    }
    
    public void addRoom(Room r)
    {
        rooms.add(r);
    }
    public Room getRoom (int i)
    {
        return rooms.get(i);
    }
}
